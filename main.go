package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sort"
	"strings"
	"time"

	"github.com/colega/envconfig"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

type config struct {
	MastodonAccessToken string `envconfig:"MASTODON_ACCESS_TOKEN" required:"true"`
	MastodonDomain      string `envconfig:"MASTODON_DOMAIN" required:"true"`
	MastodonURL         string `envconfig:"MASTODON_URL" required:"true"`

	// KubernetesNamespace is the namespace where the secret and deployment are located.
	KubernetesNamespace string `envconfig:"KUBERNETES_NAMESPACE" required:"true"`

	// KubernetesSecretName is the name of the secret that contains the SMF config.
	KubernetesSecretName string `envconfig:"KUBERNETES_SECRET_NAME" required:"true"`

	// KubernetesDeploymentName is the name of the deployment that has to be restarted if the config changes.
	KubernetesDeploymentName string `envconfig:"KUBERNETES_DEPLOYMENT_NAME" required:"true"`
}

const noEnvPrefix = ""
const secretConfigKey = "SMF_CONFIG" // nosemgrep: gosec.G101-1

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile | log.LUTC | log.Lmicroseconds)

	t0 := time.Now()
	defer func() { log.Printf("Finished in %s", time.Since(t0)) }()

	var cfg config
	if err := envconfig.Process(noEnvPrefix, &cfg); err != nil {
		log.Fatalf("Failed to process config: %v", err)
	}

	accounts, err := getAccounts(cfg.MastodonURL, cfg.MastodonAccessToken)
	if err != nil {
		log.Fatalf("Failed to get accounts: %v", err)
	}

	confirmed := filterConfirmedAccounts(accounts)
	log.Printf("Got %d accounts in %s, %d confirmed.", len(accounts), time.Since(t0), len(confirmed))

	sort.Slice(confirmed, func(i, j int) bool {
		return confirmed[i].Email < confirmed[j].Email
	})

	smfConfig := buildSMFConfig(confirmed, cfg)
	log.Printf("SMF config: %s", smfConfig)

	kubeConfig, err := rest.InClusterConfig()
	if err != nil {
		log.Fatalf("Failed to get kube config: %v", err)
	}

	kubeClient, err := kubernetes.NewForConfig(kubeConfig)
	if err != nil {
		log.Fatalf("Failed to create kube client: %v", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	changed, err := updateSecret(ctx, kubeClient, cfg, smfConfig)
	if err != nil {
		log.Fatalf("Failed to update secret: %v", err)
	}

	if !changed {
		log.Printf("Config didn't change, no need to restart the deployment")
		return
	}

	if err := restartDeployment(ctx, kubeClient, cfg); err != nil {
		log.Fatalf("Failed to restart deployment: %v", err)
	}
}

func updateSecret(ctx context.Context, kubeClient *kubernetes.Clientset, cfg config, smfConfig string) (changed bool, err error) {
	kubeSecrets := kubeClient.CoreV1().Secrets(cfg.KubernetesNamespace)
	secret, err := kubeSecrets.Get(ctx, cfg.KubernetesSecretName, metav1.GetOptions{})
	if apierrors.IsNotFound(err) {
		_, err := kubeSecrets.Create(ctx, &corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{Name: cfg.KubernetesSecretName},
			Data:       map[string][]byte{secretConfigKey: []byte(smfConfig)},
		}, metav1.CreateOptions{})
		if err != nil {
			return false, fmt.Errorf("failed to create secret: %w", err)
		}
		log.Printf("Created secret %s", cfg.KubernetesSecretName)
		changed = true
	} else if err != nil {
		return false, fmt.Errorf("failed to get secret: %w", err)
	} else if string(secret.Data[secretConfigKey]) != smfConfig {
		secret.Data[secretConfigKey] = []byte(smfConfig)
		_, err := kubeSecrets.Update(ctx, secret, metav1.UpdateOptions{})
		if err != nil {
			return false, fmt.Errorf("failed to update secret: %w", err)
		}
		log.Printf("Updated secret %s", cfg.KubernetesSecretName)
		changed = true
	} else {
		log.Printf("Secret %s is up-to-date", cfg.KubernetesSecretName)
	}
	return changed, nil
}

func restartDeployment(ctx context.Context, kubeClient *kubernetes.Clientset, cfg config) error {
	patchJSON, err := json.Marshal(map[string]any{
		"spec": map[string]any{
			"template": map[string]any{
				"metadata": map[string]any{
					"annotations": map[string]any{
						"mastodon-smf-sync-date": time.Now().Format(time.RFC3339),
					},
				},
			},
		},
	})
	if err != nil {
		return fmt.Errorf("failed to marshal deployment patch: %w", err)
	}

	_, err = kubeClient.AppsV1().
		Deployments(cfg.KubernetesNamespace).
		Patch(ctx, cfg.KubernetesDeploymentName, types.StrategicMergePatchType, patchJSON, metav1.PatchOptions{})
	if err != nil {
		return fmt.Errorf("failed to patch deployment %s: %v", cfg.KubernetesDeploymentName, err)
	}
	log.Printf("Patched deployment %s", cfg.KubernetesDeploymentName)
	return nil
}

const adminAccountsV1Path = "/api/v1/admin/accounts"

func getAccounts(mastodonURL, token string) ([]account, error) {
	accountsAPIURL := fmt.Sprintf("%s%s", mastodonURL, adminAccountsV1Path)

	req, err := http.NewRequest("GET", accountsAPIURL, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var accounts []account
	if err := json.NewDecoder(resp.Body).Decode(&accounts); err != nil {
		return nil, err
	}
	return accounts, nil
}

// account represents an account as seen by an admin.
// https://docs.joinmastodon.org/entities/Admin_Account/.
type account struct {
	Username  string `json:"username"`
	Email     string `json:"email"`
	Confirmed bool   `json:"confirmed"`
}

func filterConfirmedAccounts(accounts []account) []account {
	var confirmedAccounts []account
	for _, account := range accounts {
		if account.Confirmed {
			confirmedAccounts = append(confirmedAccounts, account)
		}
	}
	return confirmedAccounts
}

func buildSMFConfig(confirmed []account, cfg config) string {
	var smfConfigBuilder strings.Builder
	for i, account := range confirmed {
		if i > 0 {
			smfConfigBuilder.WriteByte(';')
		}

		smfConfigBuilder.WriteString(fmt.Sprintf("%s@%s", account.Username, cfg.MastodonDomain))
		smfConfigBuilder.WriteByte(':')
		smfConfigBuilder.WriteString(account.Email)
	}
	smfConfig := smfConfigBuilder.String()
	return smfConfig
}
