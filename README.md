# Mastodon - SimpleMailForwarder Sync

This is a simple application that sychronizes Mastodon users to [Simple Mail Forwarder](https://github.com/huan/docker-simple-mail-forwarder) config.

While proper docs are still in progress, you can see [an example of deployment as a Kubernetes CronJob in tu-social](https://gitlab.com/tu-social/tu-social/-/blob/9cd8a8bc65ba902c7eb0162264abd755abdc96be/jsonnet/environments/tu-social/simple-mail-forwarder.libsonnet).